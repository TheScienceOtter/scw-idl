set_sunbird.pro sets up the Solarsoft environment for IDL on Supercomputing Wales.

To set up IDl for usage with SDO data, run the following at the start of the session:

`set_sunbird, ['sdo']`

Example of usage can be found on the SCW website: https://portal.supercomputing.wales/index.php/running-idl-jobs/ 