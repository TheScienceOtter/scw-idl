#!/bin/bash 
#SBATCH -n 2                     #Number of processors in our pool. This will run 2 IDL jobs in parallel
#SBATCH --mem-per-cpu=100#Mb	#Maximum memory per CPU, set here at 100Mb
#SBATCH --account=scw1091       #Account number (so job is associated with a research project, ask Colin if unsure)
#SBATCH --time=3-00:00#days-hr:mn give overestimate, 3 days is maximum possible in any case

module purge        #Purge any loaded modules
module load parallel    #Load parallel module

# Define srun arguments:
srun="srun -n1 -N1 --exclusive"
# --exclusive     ensures srun uses distinct CPUs for each job step
# -N1 -n1         allocates a single core to each task

# Define parallel arguments:
parallel="parallel -N 1 --delay 2.0 -j $SLURM_NTASKS --joblog parallel_joblog"
  # -N 1              is number of arguments to pass to each job
# --delay .2        prevents overloading the controlling node on short jobs
# -j $SLURM_NTASKS  is the number of concurrent tasks parallel runs, so number of CPUs allocated
  # --joblog name     parallel's log file of tasks it has run

#echo "Before parallel"

# Run the tasks: In this case it will run 16 jobs (2 at a time, see SBATCH call 2nd line above). As a job ends, it runs the next.
$parallel "$srun ./huwexample_call.sh {1}" ::: {1..16}



