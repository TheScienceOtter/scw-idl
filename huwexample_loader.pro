pro huwexample_loader,arg

;set various SSW environments using set_sunbird
set_sunbird

; take argument (the 1,2,3,4...16 given by parallel in last line of huwexample_master.sh) and do something with it
case arg of 
	1:print,'Hello'
	2:print,'Arg is 2'
	3:print,'You can specify any number of different settings according to the input arg'
	else:print,'Etc',arg
endcase


end